<?php

namespace App\Models;

use App\lib\MessageTransport\dto\DeviceInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $token
 * @property string $token_type
 *
 * @property UserMessage $sentMessages
 */
class User extends Model implements DeviceInterface
{
    const TOKEN_TYPE_ANDROID = 'android';
    const TOKEN_TYPE_IOS = 'ios';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @var array
     */
    protected $fillable = ['token', 'token_type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentMessages()
    {
        return $this->hasMany(UserMessage::class);
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getDeviceType(): string
    {
        return $this->token_type;
    }
}
