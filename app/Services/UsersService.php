<?php

namespace App\Services;

use App\Models\User;

/**
 * Class UsersService
 * @package App\Services
 */
class UsersService
{
    /**
     * @param string $token
     * @param string $tokenType
     * @return User
     */
    public function authorize(string $token, string $tokenType)
    {
        return User::firstOrCreate([
            'token' => $token,
            'token_type' => $tokenType
        ]);
    }
}