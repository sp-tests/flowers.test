<?php

namespace App\Providers;

use App\lib\MessageTransport\RandomTransport;
use App\lib\MessageTransport\TransportInterface;
use App\Services\MessagesService;
use App\Services\UsersService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        TransportInterface::class => RandomTransport::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
