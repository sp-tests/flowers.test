<?php

namespace App\Console\Commands;

use App\Services\UsersService;
use Illuminate\Console\Command;

class Authorize extends Command
{
    /**
     * @var UsersService
     */
    private $usersService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:authorize {--token=} {--tokenType=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UsersService $service)
    {
        $this->usersService = $service;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $user = $this->usersService->authorize($this->option('token'), $this->option('tokenType'));
            echo 'Successful authorization. UserId: ' . $user->id . PHP_EOL;
        } catch (\Exception $e) {
            echo "Authorization failed: " . $e->getMessage() . PHP_EOL;
        }
    }
}
