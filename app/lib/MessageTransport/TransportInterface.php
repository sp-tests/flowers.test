<?php

namespace App\lib\MessageTransport;
use App\lib\MessageTransport\dto\DeviceInterface;
use App\lib\MessageTransport\dto\MessageInterface;
use App\lib\MessageTransport\exceptions\SendMessageException;

/**
 * Interface TransportInterface
 * @package App\lib\MessageTransport
 */
interface TransportInterface
{
    /**
     * @param MessageInterface $message
     * @param DeviceInterface $recipient
     *
     * @return void
     * @throws SendMessageException
     */
    public function send(MessageInterface $message, DeviceInterface $recipient): void;
}