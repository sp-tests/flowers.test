<?php

namespace App\lib\MessageTransport\dto;

/**
 * Interface DeviceIdentifierInterface
 * @package App\lib\MessageTransport\dto
 */
interface DeviceInterface
{
    /**
     * @return string
     */
    public function getDeviceId(): string;

    /**
     * @return string
     */
    public function getDeviceType(): string;
}