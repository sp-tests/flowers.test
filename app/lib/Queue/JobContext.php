<?php

namespace App\lib\Queue;

/**
 * Class JobContext
 * @package App\lib\Queue
 */
class JobContext
{
    /**
     * @var int
     */
    public $tries;

    /**
     * @var int
     */
    public $releaseDelay = 0;

    /**
     * @var string
     */
    public $logChannel = null;
}